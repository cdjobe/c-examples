﻿using UnityEngine;
using System.Collections;

public class LoadReload : MonoBehaviour {

    
    public Cannonball cannonball;
    public SmokeFade smokepuff;
    
    private float nextUsage;
    private float delay = 0.5f; // half second delay
    private bool hasfired = false;
    
    void Awake() {
    }

    // Use this for initialization
	void Start () {
        nextUsage = Time.time + delay;
    }
	
	// Update is called once per frame
	void Update () {
    
        if (Time.time > nextUsage) {
            if (Input.GetMouseButtonDown(0)) {
                    
                Cannonball clone = (Cannonball)Instantiate(cannonball);
                clone.transform.parent = transform;
                clone.transform.localPosition = new Vector3(8f,1.6f,0);
                
                SmokeFade smokeClone = (SmokeFade)Instantiate(smokepuff);
                smokeClone.transform.parent = transform;
                
                nextUsage = Time.time + delay;
            
            }
	}   }
}
