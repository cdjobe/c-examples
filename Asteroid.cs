﻿using UnityEngine;
using System.Collections;

public class Asteroid : MonoBehaviour {

	
    public GameObject explosion;
    
    private Rigidbody2D rigidBody;
    private Transform asteroid;
    private float randSpeed = Random.Range(25f, 300f);
    private float randVelocity = Random.Range(-50f, 50f);
    private float randScale = Random.Range(.25f, 1.1f);
    
    // Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.AddForce(Vector3.down * randSpeed);
        rigidBody.angularVelocity = randVelocity;
        transform.localScale = new Vector3(randScale, randScale, 1); // allows random scaling of asteroid
    
        asteroid = GetComponent<Transform>();
    }
	
	void FixedUpdate() {      
    }
    
    // Update is called once per frame
	    void Update () {
       
	}
    
    /*
    First "if" kills object and replaces with explosion image.
    Second "if" kills object if it hits the respawn block outside of game scene
    */
    void OnTriggerEnter2D(Collider2D col) {
        
        if (col.tag == "Player") {
            GameObject explosionClone = (GameObject)Instantiate(explosion, asteroid.transform.position, asteroid.transform.rotation);
            explosionClone.transform.localScale = new Vector3(randScale, randScale, 1);
            Die();
        }
        if (col.tag == "Respawn") {
            Die();
        }
    }
    
    void Die() {
        Destroy(gameObject);
    }
}
