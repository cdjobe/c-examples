﻿using UnityEngine;
using System.Collections;

public class SmokeFade : MonoBehaviour {

    public float fadeSpeed = 0f;
    public float fadeTime = 0.5f;

    private Transform cannon;
    private SpriteRenderer spriteRenderer;
    private bool hasFired = false;
    
	// Use this for initialization
	void Start () {
        cannon = transform.parent;
        spriteRenderer = GetComponent<SpriteRenderer>();
        transform.localPosition = new Vector3(11f, 1.5f, 0f);
        transform.rotation = cannon.rotation;
    }
	
	// Update is called once per frame
	void Update () {
        
        float fade = Mathf.SmoothDamp(
            spriteRenderer.color.a, 0f, ref fadeSpeed, fadeTime);
        spriteRenderer.color = new Color (1f, 1f, 1f, fade);
                
	}
}
