﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

    float maxSpeed = 8f;
    public float shipBoundary = 0.3f;
    
    // Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {

    float move = Input.GetAxis ("Horizontal");

    Vector2 position = transform.position;
    GetComponent<Rigidbody2D>().velocity = new Vector2(move * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
    
    
    // Restrict player to camera x boundaries
    float screenRatio = (float)Screen.width / (float)Screen.height;
    float widthOrtho = Camera.main.orthographicSize * screenRatio;
    
    if (position.x + shipBoundary > widthOrtho) {
        position.x = widthOrtho - shipBoundary;
    }        
    if (position.x - shipBoundary < -widthOrtho) {
        position.x = -widthOrtho + shipBoundary;
    }
    
    transform.position = position;
    
    }
}
