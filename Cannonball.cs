﻿using UnityEngine;
using System.Collections;

public class Cannonball : MonoBehaviour {

	public float shotForce = 400;

	private Rigidbody2D rigidBody;
	private AudioSource audioSource;
	public Transform cannon;
	private bool hasFired = false;

	public void Start() {
		rigidBody = GetComponent<Rigidbody2D> ();
		audioSource = GetComponent<AudioSource> ();
		cannon = transform.parent;
        rigidBody.gravityScale = 1f;
		rigidBody.AddForce(cannon.right * shotForce);
		audioSource.Play ();
    }

	// Update is called once per frame
	void Update() {
        
    }
}
