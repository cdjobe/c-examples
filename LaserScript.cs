﻿using UnityEngine;
using System.Collections;

public class LaserScript : MonoBehaviour {

    private LineRenderer laserLine;
    public GameObject laser;
    public GameObject laserGun;
    
    
	// Use this for initialization
	void Start ()
    {
        laserLine = laser.GetComponent<LineRenderer>();
        laserLine.enabled = false;
        
        // Screen.lockCursor = true; // disable mouse cursor
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButtonDown ("Fire1"))
        {
            StopCoroutine("FireLaser");
            StartCoroutine("FireLaser");
        }
    }
    
    IEnumerator FireLaser ()
    {
        GameObject laserClone = (GameObject) Instantiate(laser,laserGun.transform.position,laserGun.transform.rotation);
        laserClone.GetComponent<Rigidbody>().AddForce(transform.forward * 5000);
        
        laserLine = laserClone.GetComponent<LineRenderer>();
        laserLine.enabled = true;
        
        while (Input.GetButton("Fire1"))
        {
            Ray ray = new Ray (transform.position, transform.forward);
            RaycastHit hit;
          
            laserLine.SetPosition (0, ray.origin);
            laserLine.SetPosition (1, ray.GetPoint(100));
            
            if (Physics.Raycast(ray, out hit, 500))
            {
                laserLine.SetPosition (1, hit.point);
                
                if (hit.rigidbody)
                {
                   laserClone.GetComponent<Rigidbody>().position = hit.point;
                   hit.rigidbody.AddForceAtPosition (transform.forward*20, hit.point);
                }
            }
            else
                laserLine.SetPosition(1, ray.GetPoint(500));
            
            yield return null;
        }
        
        laserLine.enabled = false;
        Destroy(laserClone);
    }
}





