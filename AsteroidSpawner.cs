﻿using UnityEngine;
using System.Collections;

public class AsteroidSpawner : MonoBehaviour {

    public Asteroid asteroid;
    
    private Rigidbody2D rigidBody;
    private float nextUsage;
    private float delay = 1f;
        
    // Use this for initialization
	public void Start () {
        nextUsage = Time.time + delay;
    }
	
	// Update is called once per frame
	void Update () {
	
        if (Time.time > nextUsage) {
            
            Vector3 asteroidPosition = new Vector3(Random.Range(-8f, 8f), transform.position.y, transform.position.z);
            Asteroid clone = (Asteroid)Instantiate(asteroid, asteroidPosition, transform.rotation);
            
            
            nextUsage = Time.time + delay;
        
        }
    
	}
}
